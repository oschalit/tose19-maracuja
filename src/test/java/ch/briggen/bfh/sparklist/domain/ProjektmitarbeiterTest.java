package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ProjektmitarbeiterTest {

	//Testet ob ein Objekt in die Datenbank geschrieben werden kann

	@Test
	void testToString() {
		Projektmitarbeiter i = new Projektmitarbeiter();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
	
	
	@Test
	void testNachname() {
		Projektmitarbeiter i = new Projektmitarbeiter(5678, "Peter", "Hodler", "pesche@hotmail.com", "Marketing", "Hingerungerfultigen");
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString().contains("Hodler"));
	}
}
