package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.*;


import java.util.Collection;
import java.util.NoSuchElementException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class ProjektmitarbeiterRepositoryTest {

	ProjektmitarbeiterRepository repo = null;

	
	private static void populateRepo(ProjektmitarbeiterRepository r) {
		for(int i = 1; i <11; ++i) {
			// warum sind referenzen auf Abteilung und Arbeitsort Strings?
			// warum ist die id ignorisiert?
			
			//Testet anhand des Vornamens, ob einem Objekt Werte mitgegeben werden können
			//und diese dann aus der Datenbank ausgelesen werden können
			Projektmitarbeiter dummy = new Projektmitarbeiter(i, "Peter", "Hodler", "pesche@hotmail.com", "2", "3");

			r.insert(dummy);
			
			Projektmitarbeiter result = r.getById(i);
			assertThat("insert erfolgreich", result.getVorname().equals("Peter"));
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ProjektmitarbeiterRepository();
	}
	//Testet, ob eine Datenbank bei Erstellung leer ist
	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	//Testet, ob 10 Einträge in eine Datenbank eingefügt werden können
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		assertThat("Freshly populated DB must hold 10 items",repo.getAll().size(),is(10));
	}	
	
	@ParameterizedTest
	@CsvSource({
		"1,Hans,Wurst,hans@wurst.ch,3,1",
		"2,Peter,Lee,peter@lee.ch,2,2",
		"3,Kurt,Hablützel,a@b.ch,1,3",
		"4,Abe,Cede,abe@cede.ch,3,1",
	
	})
	
	//Testet, ob in der Datenbank Objekte mit allen gewünschten Einträgen erstellt werden können
	void testInsertItems(long id,String vorname, String nachname, String email, String abteilung, String arbeitsort)
	{
		populateRepo(repo);
		
		Projektmitarbeiter dummy2 = new Projektmitarbeiter(id, vorname,  nachname,  email,  abteilung,  arbeitsort);
		long dbId = repo.insert(dummy2);
		Projektmitarbeiter fromDB = repo.getById(dbId);
		
		//AbteilungRepository ar = new AbteilungRepository();
		// getById nicht implementiert 
		//ar.getById(abteilung)
		// ID übergeben wird nicht in Datenbank eingefügt
		
		//		assertThat("id = id", fromDB.getId(),is(id) );
		assertThat("vorname = vorname", fromDB.getVorname(),is(vorname) );
		assertThat("name = name", fromDB.getNachname(),is(nachname) );
		assertThat("email = email", fromDB.getEmail(),is(email) );
		assertThat("abteilung = abteilung", fromDB.getAbteilungID(),is(new Long (abteilung)) );
		assertThat("arbeitsort = arbeitsort", fromDB.getArbeitsortID(),is(new Long (arbeitsort)) );

	}
	

	@ParameterizedTest
	@CsvSource({		
		"100,Hans,Wurst,hans@wurst.ch,3,1,Salami",
		"200,Peter,Lee,peter@lee.ch,2,2,Hans",
		"300,Kurt,Hablützel,a@b.ch,1,3,Lewis"})
	
	
	//Testet, ob Einträge angepasst werden können
	//(Um andere Werte als den Vornamen zu testen, können andere Werte einkommentiert werden)
	
	void testUpdateItems(long id,String vorname, String nachname, String email, String abteilung, String arbeitsort, String newVorname)
	{
		populateRepo(repo);
		
		Projektmitarbeiter i = new Projektmitarbeiter(0, vorname,  nachname,  email,  abteilung,  arbeitsort);
		long dbId = repo.insert(i);
		
		i.setId(dbId);
		i.setVorname(newVorname);
		//i.setName(newNachname);
		//i.setEmail(newEmail);
		//i.setAbteilung(newAbteilung);
		//i.setArbeitsort(newArbeitsort);
		repo.save(i);
		
		Projektmitarbeiter fromDB = repo.getById(dbId);
		//		assertThat("id = id", fromDB.getId(),is(id) );
		assertThat("vorname = vorname", fromDB.getVorname(),is(newVorname) );
		//assertThat("name = name", fromDB.getNachname(),is(newNachname) );
		//assertThat("email = email", fromDB.getEmail(),is(email) );
		//assertThat("abteilung = abteilung", fromDB.getAbteilungID(),is(new Long (abteilung)) );
		//assertThat("arbeitsort = arbeitsort", fromDB.getArbeitsortID(),is(new Long (arbeitsort)) );
	}

	@ParameterizedTest
	@CsvSource({
		"1,Hans,Wurst,hans@wurst.ch,3,1",
		"2,Peter,Lee,peter@lee.ch,2,2",
		"3,Kurt,Hablützel,a@b.ch,1,3"})
	
	
	//Testet, ob Werte aus der Datenbank gelöscht werden können
	void testDeleteItems(long id,String vorname, String nachname, String email, String abteilung, String arbeitsort)
	{
		populateRepo(repo);
		
		Projektmitarbeiter i = new Projektmitarbeiter(id, vorname,  nachname,  email,  abteilung,  arbeitsort);
		long dbId = repo.insert(i);
		Projektmitarbeiter fromDB = repo.getById(dbId);
		assertThat("Item was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{repo.getById(dbId);},"Item should have been deleted");
	}
	
	
	//Testet, ob alle Einträge gleichzeitig aus der Datenbank gelöscht werden können
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Projektmitarbeiter i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}

}