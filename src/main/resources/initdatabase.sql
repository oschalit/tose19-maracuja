-- drop table projektmitarbeiter if exists cascade;
-- drop table projekt if exists cascade;
-- drop table projektma_projekt if exists cascade;

create table if not exists abteilung (
	aid long identity,
	name varchar(200) not null default '1'
);

create table if not exists arbeitsort (
	aoid long identity,
	name varchar(200) not null default '1'
);

insert into abteilung (name) select 'Informatik' where not exists (select 1 from abteilung where name = 'Informatik');
insert into abteilung (name) select 'Marketing' where not exists (select 1 from abteilung where name = 'Marketing');
insert into abteilung (name) select 'Verkauf' where not exists (select 1 from abteilung where name = 'Verkauf');
insert into abteilung (name) select 'Finanzen' where not exists (select 1 from abteilung where name = 'Finanzen');
insert into abteilung (name) select 'HR' where not exists (select 1 from abteilung where name = 'HR');
insert into abteilung (name) select 'Rechtsabteilung' where not exists (select 1 from abteilung where name = 'Rechtsabteilung');
insert into abteilung (name) select 'Logistik' where not exists (select 1 from abteilung where name = 'Logistik');
insert into abteilung (name) select 'Extern' where not exists (select 1 from abteilung where name = 'Extern');

insert into arbeitsort (name) select 'Bern' where not exists (select 1 from arbeitsort where name = 'Bern');
insert into arbeitsort (name) select 'Basel' where not exists (select 1 from arbeitsort where name = 'Basel');
insert into arbeitsort (name) select 'Winterthur' where not exists (select 1 from arbeitsort where name = 'Winterthur');

create table if not exists projektmitarbeiter (
	pmid long identity,
	vorname varchar(200) not null,
	nachname varchar(200) not null,
	email varchar(200) not null,
	abteilung varchar(200) not null default '0',
	arbeitsort varchar(200) not null default '0', 
	foreign key(abteilung) references abteilung(aid) on delete set default,
	foreign key(arbeitsort) references arbeitsort(aoid) on delete set default
);


-- delete from abteilung where aid='0';
-- insert into abteilung(aid, name) values ('0','Nicht definiert');

delete from projektmitarbeiter where pmid='0';
insert into projektmitarbeiter(pmid,vorname,nachname,email,abteilung,arbeitsort) values ('0','Nicht','definiert','dummy','1','1');

create table if not exists projekt (
	pid long identity,
	name varchar(200) not null,
	start date not null,
	ende date not null,
	projektleitung long not null default '0',
	foreign key(projektleitung) references projektmitarbeiter(pmid) on delete set default
);



create table if not exists projektma_projekt (
	pmid long,
	pid long,
	foreign key(pmid) references projektmitarbeiter(pmid) on delete cascade,
	foreign key(pid) references projekt(pid) on delete cascade
);

-- insert into projektmitarbeiter(vorname,nachname,email,abteilung) values ('Peter','Meier','peter.meier@abcde.fg','Marketing');
-- insert into projektmitarbeiter(vorname,nachname,email,abteilung) values ('Hans','Gerber','hans.gerber@abcde.fg','Finanzen');

-- insert into projekt(name,start,ende,projektleitung) values ('IT-Projekt','2018-01-30','2018-06-30',1);
-- insert into projekt(name,start,ende,projektleitung) values ('Marketing-Projekt','2019-01-01','2019-05-31',2);

-- insert into projektma_projekt(pmid,pid) select min(pm.pmid), min(p.pid) from projektmitarbeiter pm, projekt p;
-- insert into projektma_projekt(pmid,pid) select max(pm.pmid), max(p.pid) from projektmitarbeiter pm, projekt p;

-- select p.pid, p.name, p.start, p.ende, pm.vorname, pm.nachname
-- from projekt p
-- join projektmitarbeiter pm on p.pid = pm.pmid;

-- select projektleitung as "Noch nicht bekannt" from projekt where projektleitung not in (select pmid from projektmitarbeiter);