package ch.briggen.bfh.sparklist;

import static spark.Spark.get;


import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.PMitarbeiterProjektHinzufuegenController;
import ch.briggen.bfh.sparklist.web.PMitarbeiterProjektLoeschenController;
import ch.briggen.bfh.sparklist.web.ProjektAktualisierenController;
import ch.briggen.bfh.sparklist.web.ProjektAnzeigenController;
import ch.briggen.bfh.sparklist.web.ProjektBearbeitenController;
import ch.briggen.bfh.sparklist.web.ProjektLesenController;
import ch.briggen.bfh.sparklist.web.ProjektLoeschenController;
import ch.briggen.bfh.sparklist.web.ProjektNeuController;
import ch.briggen.bfh.sparklist.web.ProjektPMBearbeitenController;
import ch.briggen.bfh.sparklist.web.ProjektmitarbeiterAktualisierenController;
import ch.briggen.bfh.sparklist.web.ProjektmitarbeiterVRootController;
import ch.briggen.bfh.sparklist.web.ProjektmitarbeiterAnzeigenController;
import ch.briggen.bfh.sparklist.web.ProjektmitarbeiterBearbeitenController;
import ch.briggen.bfh.sparklist.web.ProjektmitarbeiterLoeschenController;
import ch.briggen.bfh.sparklist.web.ProjektmitarbeiterNeuController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

/**
 * Konfiguriert und startet Server und beinhaltet HTTP Handler
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new ProjektmitarbeiterVRootController(), new UTF8ThymeleafTemplateEngine());
		get("/projektmitarbeiterAnzeigen", new ProjektmitarbeiterAnzeigenController(), new UTF8ThymeleafTemplateEngine());
		get("/projektmitarbeiter", new ProjektmitarbeiterBearbeitenController(), new UTF8ThymeleafTemplateEngine());
		post("/projektmitarbeiter/neu", new ProjektmitarbeiterNeuController(), new UTF8ThymeleafTemplateEngine());
		post("/projektmitarbeiter/aktualisieren", new ProjektmitarbeiterAktualisierenController(), new UTF8ThymeleafTemplateEngine());
		get("/projektmitarbeiter/loeschen", new ProjektmitarbeiterLoeschenController(), new UTF8ThymeleafTemplateEngine());
		get("/pmitarbeiterprojekt/loeschen", new PMitarbeiterProjektLoeschenController(), new UTF8ThymeleafTemplateEngine());
		get("/pmitarbeiterprojekt/hinzufuegen", new PMitarbeiterProjektHinzufuegenController(), new UTF8ThymeleafTemplateEngine());
		get("/projektpmbearbeiten", new ProjektPMBearbeitenController(), new UTF8ThymeleafTemplateEngine());
		get("/projektAnzeigen", new ProjektAnzeigenController(), new UTF8ThymeleafTemplateEngine());
		get("/projekt", new ProjektBearbeitenController(), new UTF8ThymeleafTemplateEngine());
		get("/projektlesen", new ProjektLesenController(), new UTF8ThymeleafTemplateEngine());
		post("/projekt/neu", new ProjektNeuController(), new UTF8ThymeleafTemplateEngine());
		post("/projekt/aktualisieren", new ProjektAktualisierenController(), new UTF8ThymeleafTemplateEngine());
		get("/projekt/loeschen", new ProjektLoeschenController(), new UTF8ThymeleafTemplateEngine());

	}

}
