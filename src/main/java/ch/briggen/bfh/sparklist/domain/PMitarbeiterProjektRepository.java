package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle DB Operationen, welche Mitarbeiter UND Projekte betreffen. 
 * @author Marcel Briggen @editor Team Maracuja
 *
 */


public class PMitarbeiterProjektRepository {
	
	private final static Logger log = LoggerFactory.getLogger(PMitarbeiterProjektRepository.class);
	
	/**
	 * Löscht Zuweisung von Projekt und Projektmitarbeiter von genannten IDs
	 * @param pmid Projektmitarbeiter ID
	 * @param pid Projekt ID
	 */
	public static void delete(long pmid, long pid) {
		log.trace("delete " + pmid + pid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektma_projekt where pmid= ? and pid = ?");
			stmt.setLong(1, pmid);
			stmt.setLong(2, pid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing items by id " + pmid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}
		
	/**
	 * Zuweisung von Projekt und Projektmitarbeiter von genannten IDs
	 * @param pmid Projektmitarbeiter ID
	 * @param pid Projekt ID
	 */
	public static void insert(long pmid, long pid) {
		log.trace("insert " + pmid + pid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projektma_projekt (pmid,pid) values (?,?);");
			stmt.setLong(1, pmid);
			stmt.setLong(2, pid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing items by id " + pmid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	
}


