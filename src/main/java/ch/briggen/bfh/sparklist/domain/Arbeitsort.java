package ch.briggen.bfh.sparklist.domain;


/**
 * Arbeitsort Klasse, dient zum Instanzieren von Arbeitsort Objekten.
 * @author Marcel Briggen @editor Team Maracuja
 *
 */
public class Arbeitsort {
	
	private long id;
	private String name;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Arbeitsort()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id eindeutige Id des Arbeitsorts
	 * @param name Name des Arbeitsorts
	 */
	public Arbeitsort(long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Arbeitsort:{id: %d; name: %s}", id, name);
	}
	
}
