package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Arbeitsorte. 
 * Hier werden alle Funktionen für die DB-Operationen für Arbeitsorte implementiert.
 * @author Marcel Briggen @edited Team Maracuja
 *
 */


public class ArbeitsortRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ArbeitsortRepository.class);
	

	/**
	 * Liefert alle Arbeitsorte in der Datenbank
	 * @return Collection aller Arbeitsorte
	 */
	public static Collection<Arbeitsort> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select aoid,name from Arbeitsort where aoid !='0'");
			ResultSet rs = stmt.executeQuery();
			return mapArbeitsorte(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Arbeitsorte in der Datenbank, ausser Arbeitsort von mitgegebener Arbeitsort ID
	 * @return Collection aller Arbeitsorte in der Datenbank, ausser Areitsort von mitgegebener Arbeitsort ID
	 */
	public static Collection<Arbeitsort> getAlmostAll(long id)  {
		log.trace("getAlmostAll" + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select distinct ao.aoid,ao.name from Arbeitsort ao left join projektmitarbeiter pm on pm.arbeitsort = ao.aoid where aoid != ? or pmid is null order by name");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapArbeitsorte(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Arbeitsort-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen @editor Team Maracuja
	 * @throws SQLException 
	 *
	 */
	private static Collection<Arbeitsort> mapArbeitsorte(ResultSet rs) throws SQLException 
	{
		LinkedList<Arbeitsort> arbeitsortListe = new LinkedList<Arbeitsort>();
		while(rs.next())
		{
			Arbeitsort i = new Arbeitsort(rs.getLong("aoid"),rs.getString("name"));
			arbeitsortListe.add(i);
		}
		return arbeitsortListe;
	}

}
