package ch.briggen.bfh.sparklist.domain;


/**
 * Klasse Abteilung. Dient zum Instanzieren von Abteilungsobjekten.
 * @author Marcel Briggen @editor Team Maracuja
 *
 */
public class Abteilung {
	
	private long id;
	private String name;
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Abteilung()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id eindeutige Id
	 * @param name Name der Abteilung
	 */
	public Abteilung(long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Abteilung:{id: %d; name: %s}", id, name);
	}
	
}
