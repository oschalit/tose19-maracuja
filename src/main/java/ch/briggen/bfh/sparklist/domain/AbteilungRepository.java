package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Abteilungen. 
 * Hier werden alle Funktionen für die DB-Operationen für Abteilungen implementiert.
 * @author Marcel Briggen @edited Team Maracuja
 *
 */


public class AbteilungRepository {
	
	private final static Logger log = LoggerFactory.getLogger(AbteilungRepository.class);
	

	/**
	 * Liefert alle Abteilungen in der Datenbank
	 * @return Collection aller Abteilungen
	 */
	public static Collection<Abteilung> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select aid,name from Abteilung where aid !='0'");
			ResultSet rs = stmt.executeQuery();
			return mapAbteilungen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Projektmitarbeiter in der Datenbank, ausser Projektleiter von mitgegebener Projekt ID
	 * @return Collection aller Projektmitarbeiter in der Datenbank, ausser Projektleiter von mitgegebener Projekt ID
	 */
	public static Collection<Abteilung> getAlmostAll(long id)  {
		log.trace("getAlmostAll" + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select distinct a.aid,a.name from Abteilung a left join projektmitarbeiter pm on pm.abteilung = a.aid where aid != ? or pmid is null order by name");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapAbteilungen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Abteilung-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen @editor Team Maracuja
	 * @throws SQLException 
	 *
	 */
	private static Collection<Abteilung> mapAbteilungen(ResultSet rs) throws SQLException 
	{
		LinkedList<Abteilung> abteilungListe = new LinkedList<Abteilung>();
		while(rs.next())
		{
			Abteilung i = new Abteilung(rs.getLong("aid"),rs.getString("name"));
			abteilungListe.add(i);
		}
		return abteilungListe;
	}

}
