package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projektmitarbeiter. 
 * Hier werden alle Funktionen für die DB-Operationen für Projektmitarbeiter implementiert.
 * @author Marcel Briggen @edited Team Maracuja
 *
 */


public class ProjektmitarbeiterRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ProjektmitarbeiterRepository.class);
	

	/**
	 * Liefert alle Projektmitarbeiter in der Datenbank, ausser der Default "nicht definiert" dummy User mit der ID 0
	 * @return Collection aller Projektmitarbeiter, ausser dummy mit ID 0
	 */
	public static Collection<Projektmitarbeiter> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select pm.pmid, pm.vorname, pm.nachname, pm.email, a.name as Abteilung, ao.name as Arbeitsort from projektmitarbeiter pm join abteilung a on pm.abteilung = a.aid join arbeitsort ao on pm.arbeitsort = ao.aoid where pmid != '0'");
			ResultSet rs = stmt.executeQuery();
			return mapProjektmitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle Projektmitarbeiter in der Datenbank, ausser Projektleiter von mitgegebener Projekt ID
	 * @return Collection aller Projektmitarbeiter in der Datenbank, ausser Projektleiter von mitgegebener Projekt ID
	 */
	public static Collection<Projektmitarbeiter> getAlmostAll(long id)  {
		log.trace("getAlmostAll" + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select distinct pm.pmid, pm.vorname, pm.nachname, pm.email, a.name as abteilung, ao.name as Arbeitsort from projektmitarbeiter pm left join projekt p on pm.pmid=p.projektleitung join abteilung a on pm.abteilung = a.aid join arbeitsort ao on pm.arbeitsort = ao.aoid where pm.pmid not in (select distinct pm.pmid from projektmitarbeiter pm join projekt p on pm.pmid=p.projektleitung where pid = ?) order by pmid");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektmitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle Projektmitarbeiter in der Datenbank, welche in Projekt id sind
	 * @return Collection aller Projektmitarbeiter in der Datenbank, welche in Projekt id sind
	 */
	public static Collection<Projektmitarbeiter> getFromProject(long id)  {
		log.trace("getFromProjekt" + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select distinct pm.pmid, pm.vorname, pm.nachname, pm.email, a.name as abteilung, ao.name as Arbeitsort from projektmitarbeiter pm join projektma_projekt pmp on pm.pmid = pmp.pmid join abteilung a on pm.abteilung = a.aid join arbeitsort ao on pm.arbeitsort = ao.aoid where pid = ?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektmitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle Projektmitarbeiter in der Datenbank, welche nicht in Projekt id sind
	 * @return Collection aller Projektmitarbeiter in der Datenbank, welche nicht in Projekt id sind
	 */
	public static Collection<Projektmitarbeiter> notInProject(long id)  {
		log.trace("notInProject" + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select distinct pm.pmid, pm.vorname, pm.nachname, pm.email, a.name as abteilung, ao.name as Arbeitsort from projektmitarbeiter pm join abteilung a on pm.abteilung = a.aid join arbeitsort ao on pm.arbeitsort = ao.aoid where pm.pmid  not in (select distinct pm.pmid from projektmitarbeiter pm join projektma_projekt pmp on pm.pmid = pmp.pmid join abteilung a on pm.abteilung = a.aid join arbeitsort ao on pm.arbeitsort = ao.aoid where pid = ?) and pm.pmid != '0'");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektmitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	
	/**
	 * Liefert Projektmitarbeiter mit bestimmter ID zurück
	 * @return Liefert Projektmitarbeiter mit bestimmter ID zurück
	 */
	public static Projektmitarbeiter getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select pm.pmid, pm.vorname, pm.nachname, pm.email, pm.abteilung, a.name as abteilungname, pm.arbeitsort, ao.name as arbeitsortname from projektmitarbeiter pm join abteilung a on pm.abteilung = a.aid join arbeitsort ao on pm.arbeitsort = ao.aoid where pmid = ?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektmitarbeiter2(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	
	/**
	 * Insert Methode um neues Projektmitarbeiter Objekt in die DB einzufügen
	 * @return ID von einefügtem Projektmitarbeiter
	 */
	public long insert(Projektmitarbeiter i) {
		
		log.trace("insert " + i);
		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projektmitarbeiter (vorname, nachname, email, abteilung, arbeitsort) values (?,?,?,?,?)");
			stmt.setString(1, i.getVorname());
			stmt.setString(2, i.getNachname());
			stmt.setString(3, i.getEmail());
			stmt.setString(4, i.getAbteilung());
			stmt.setString(5, i.getArbeitsort());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Speichert den übergebenen Projektmitarbeiter in der Datenbank. UPDATE.
	 * @param i Projektmitarbeiter ID
	 */
	public static void save(Projektmitarbeiter i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projektmitarbeiter set vorname=?, nachname=?, email=?, abteilung=?, arbeitsort=? where pmid=?");
			stmt.setString(1, i.getVorname());
			stmt.setString(2, i.getNachname());
			stmt.setString(3, i.getEmail());
			stmt.setString(4, i.getAbteilung());
			stmt.setString(5, i.getArbeitsort());
			stmt.setLong(6, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	
	/**
	 * Löscht den Projektmitarbeiter mit der angegebenen Id von der DB
	 * @param id Projektmitarbeiter ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektmitarbeiter where pmid=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	
	/**
	 * Helper zum konvertieren der Resultsets in Projektmitarbeiter-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen @editor Team Maracuja
	 * @throws SQLException 
	 *
	 */
	private static Collection<Projektmitarbeiter> mapProjektmitarbeiter(ResultSet rs) throws SQLException 
	{
		LinkedList<Projektmitarbeiter> projektmitarbeiterListe = new LinkedList<Projektmitarbeiter>();
		while(rs.next())
		{
			Projektmitarbeiter i = new Projektmitarbeiter(rs.getLong("pmid"),rs.getString("vorname"),rs.getString("nachname"),rs.getString("email"),rs.getString("abteilung"),rs.getString("arbeitsort"));
			projektmitarbeiterListe.add(i);
		}
		return projektmitarbeiterListe;
	}
	
	private static Collection<Projektmitarbeiter> mapProjektmitarbeiter2(ResultSet rs) throws SQLException 
	{
		LinkedList<Projektmitarbeiter> projektmitarbeiterListe = new LinkedList<Projektmitarbeiter>();
		while(rs.next())
		{
			Projektmitarbeiter i = new Projektmitarbeiter(rs.getLong("pmid"),rs.getString("vorname"),rs.getString("nachname"),rs.getString("email"),rs.getLong("abteilung"),rs.getString("abteilungname"),rs.getLong("arbeitsort"),rs.getString("arbeitsortname"));
			projektmitarbeiterListe.add(i);
		}
		return projektmitarbeiterListe;
	}
	

}
