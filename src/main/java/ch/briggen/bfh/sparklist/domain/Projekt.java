package ch.briggen.bfh.sparklist.domain;


/**
 * Klasse Projekt, welche für das Instanzieren von Projekt Objekten benötigt wird.
 * @author Marcel Briggen @editor Team Maracuja
 *
 */
public class Projekt {
	
	private long id;
	private String name;
	private String startdatum;
	private String enddatum;
	private String plVorname;
	private String plNachname;
	private long projektleiter;
	private String plDisplayName;
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Projekt()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Projekts
	 * @param startdatum Startdatum des Projekts, Format yyyy-mm-dd
	 * @param enddatum Enddatum des Projekts, Format yyyy-mm-dd
	 * @param projektleiter ID des Projektleiters
	 */
	public Projekt(long id, String name, String startdatum, String enddatum, long projektleiter)
	{
		this.id = id;
		this.name = name;
		this.startdatum = startdatum;
		this.enddatum = enddatum;
		this.projektleiter = projektleiter;

	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Projekts
	 * @param startdatum Startdatum des Projekts, Format yyyy-mm-dd
	 * @param enddatum Enddatum des Projekts, Format yyyy-mm-dd
	 * @param projektleiter ID des Projektleiters
	 * @param plVorname Vorname von Projektleiter
	 * @param plNachname Nachname von Projektleiter
	 * Der Display Name bildet den eindeutigen verständlichen Identifier vom Projektleiter.
	 */
	public Projekt(long id, String name, String startdatum, String enddatum, long projektleiter, String plVorname, String plNachname)
	{
		this.id = id;
		this.name = name;
		this.startdatum = startdatum;
		this.enddatum = enddatum;
		this.projektleiter = projektleiter;
		this.plVorname = plVorname;
		this.plNachname = plNachname;
		
		if (projektleiter==0) {
			this.plDisplayName = "Nicht definiert";
		} else {
			this.plDisplayName = plVorname + "." + plNachname + "." + projektleiter;	
		}
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartdatum() {
		return startdatum;
	}

	public void setStartdatum(String startdatum) {
		this.startdatum = startdatum;
	}
	
	public String getEnddatum() {
		return enddatum;
	}

	public void setEnddatum(String enddatum) {
		this.enddatum = enddatum;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getPlVorname() {
		return plVorname;
	}

	public void setPlVorname(String plVorname) {
		this.plVorname = plVorname;
	}
	
	public String getPlNachname() {
		return plNachname;
	}

	public void setPlNachname(String plNachname) {
		this.plNachname = plNachname;
	}
	
	public String getPlDisplayName() {
		return plDisplayName;
	}

	public void setPlDisplayName(String plDisplayName) {
		this.plDisplayName = plDisplayName;
	}
	
	public long getProjektleiter() {
		return projektleiter;
	}

	public void setProjektleiter(long projektleiter) {
		this.projektleiter = projektleiter;
	}
	
	@Override
	public String toString() {
		return String.format("Projekt:{id: %d; name: %s; startdatum: %s; enddatum: %s; projektleiter: %d}", id, name, startdatum, enddatum, projektleiter);
	}
	

}
