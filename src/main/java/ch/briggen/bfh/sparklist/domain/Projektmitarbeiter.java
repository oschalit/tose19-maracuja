package ch.briggen.bfh.sparklist.domain;


/**
 * Projektmitarbeiter Klasse, welche zum Instanzieren von Projektmitarbeiter Objekten benötigt wird.
 * @author Marcel Briggen @edited Team Maracuja
 *
 */
public class Projektmitarbeiter {
	
	private long id;
	private String vorname;
	private String nachname;
	private String email;
	private String abteilung;
	private String pmDisplayName;
	private long abteilungID;
	private String arbeitsort;
	private long arbeitsortID;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Projektmitarbeiter()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param vorname Vorname von ProjektmitarbeiterIn
	 * @param nachname Nachname von ProjektmiarbeiterIn
	 * @param email Email Adresse von ProjektmitarbeiterIn
	 * @param abteilung Abteilung in der ProjektmitarbeiterIn tätig ist
	 * @param arbeitsort Arbeitsort in welchem der ProjektmitarbeiterIn tätig ist
	 */
	public Projektmitarbeiter(long id, String vorname, String nachname, String email, String abteilung, String arbeitsort)
	{
		this.id = id;
		this.vorname = vorname;
		this.nachname = nachname;
		this.email = email;
		this.abteilung = abteilung;
		this.arbeitsort = arbeitsort;
		
		if (id==0) {
			this.pmDisplayName = "Nicht definiert";
		} else {
			this.pmDisplayName = vorname + "." + nachname + "." + id;	
		}
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param vorname Vorname von ProjektmitarbeiterIn
	 * @param nachname Nachname von ProjektmiarbeiterIn
	 * @param email Email Adresse von ProjektmitarbeiterIn
	 * @param abteilung Abteilung ID in der ProjektmitarbeiterIn tätig ist
	 * @param abteilung Abteilungsname in der ProjektmitarbeiterIn tätig ist
	 * @param arbeitsort Arbeitsort ID in der ProjektmitarbeiterIn tätig ist
	 * @param arbeitsort Arbeitsort in der Projektmitarbeiter tätig ist
	 */
	public Projektmitarbeiter(long id, String vorname, String nachname, String email, long abteilungID, String abteilung, long arbeitsortID, String arbeitsort)
	{
		this.id = id;
		this.vorname = vorname;
		this.nachname = nachname;
		this.email = email;
		this.abteilung = abteilung;
		this.pmDisplayName = vorname + "." + nachname + "." + id;
		this.abteilungID = abteilungID;
		this.arbeitsortID = arbeitsortID;
		this.arbeitsort = arbeitsort;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAbteilung() {
		return abteilung;
	}

	public void setAbteilung(String abteilung) {
		this.abteilung = abteilung;
	}
	
	public String getPmDisplayName() {
		return pmDisplayName;
	}
	
	public long getAbteilungID() {
		return abteilungID;
	}
	
	public String getArbeitsort() {
		return arbeitsort;
	}

	public void setArbeitsort(String arbeitsort) {
		this.arbeitsort = arbeitsort;
	}


	
	@Override
	public String toString() {
		return String.format("Item:{id: %d; vorname: %s; nachname: %s; email: %s; abteilung: %s}", id, vorname, nachname, email, abteilung);
	}

	public long getArbeitsortID() {
		return arbeitsortID;
	}

	public void setArbeitsortID(long arbeitsortID) {
		this.arbeitsortID = arbeitsortID;
	}

	
	

}
