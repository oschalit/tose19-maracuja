package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projekte. 
 * Hier werden alle Funktionen für die DB-Operationen zu Projekten implementiert.
 * @author Marcel Briggen @editor Team Maracuja
 *
 */


public class ProjektRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ProjektRepository.class);
	

	/**
	 * Liefert alle Projekte in der Datenbank
	 * @return Collection aller Projekte
	 */
	public static Collection<Projekt> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select p.pid, p.name, p.start, p.ende, p.projektleitung, pm.vorname , pm.nachname from projekt p join projektmitarbeiter pm on p.projektleitung = pm.pmid");
			ResultSet rs = stmt.executeQuery();
			return mapProjekte(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Projects. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	
	/**
	 * Insert Methode um neues Projekt Objekt in die DB einzufügen
	 * @return ID von einefügtem Projekt
	 */
		public long insert(Projekt i) {
			
			log.trace("insert " + i);
			//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("insert into projekt (name, start, ende, projektleitung) values (?,?,?,?)");
				stmt.setString(1, i.getName());
				stmt.setString(2, i.getStartdatum());
				stmt.setString(3, i.getEnddatum());
				stmt.setLong(4, i.getProjektleiter());
				stmt.executeUpdate();
				ResultSet key = stmt.getGeneratedKeys();
				key.next();
				Long id = key.getLong(1);
				return id;
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating item " + i;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}

		}

		/**
		 * Löscht das Projekt mit der angegebenen Id von der DB
		 * @param id Projekt ID
		 */
		public void delete(long id) {
			log.trace("delete " + id);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("delete from projekt where pid=?");
				stmt.setLong(1, id);
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while deleteing items by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						

		}
		
		/**
		 * Speichert das übergebene Projekt in der Datenbank. UPDATE.
		 * @param i Projekt id
		 */
		public static void save(Projekt i) {
			log.trace("save " + i);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("update projekt set name=?, start=?, ende=?, projektleitung=? where pid=?");
				stmt.setString(1, i.getName());
				stmt.setString(2, i.getStartdatum());
				stmt.setString(3, i.getEnddatum());
				stmt.setLong(4, i.getProjektleiter());
				stmt.setLong(5, i.getId());
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating item " + i;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
			
		}
		
		/**
		 * Liefert Projekt mit bestimmter ID zurück
		 * @return Liefert Projekt mit bestimmter ID zurück
		 */
		public static Projekt getById(long id) {
			log.trace("getById " + id);
		
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select p.pid, p.name, p.start, p.ende, p.projektleitung, pm.vorname , pm.nachname from projekt p join projektmitarbeiter pm on p.projektleitung = pm.pmid where pid=?");
				stmt.setLong(1, id);
				ResultSet rs = stmt.executeQuery();
				return mapProjekte(rs).iterator().next();		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving items by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						
		}
	
	/**
	 * Helper zum konvertieren der Resultsets in Projekt-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @Editor	Team Maracuja
	 * @throws SQLException 
	 *
	 */
	private static Collection<Projekt> mapProjekte(ResultSet rs) throws SQLException 
	{
		LinkedList<Projekt> list = new LinkedList<Projekt>();
		while(rs.next())
		{
			Projekt i = new Projekt(rs.getLong("pid"),rs.getString("name"),rs.getString("start"),rs.getString("ende"),rs.getLong("projektleitung"),rs.getString("vorname"),rs.getString("nachname"));
			list.add(i);
		}
		return list;
	}


}


