package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller um Projekte zu aktualisieren
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektmitarbeiterAktualisierenController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjektmitarbeiterAktualisierenController.class);
		
	// private ProjektmitarbeiterRepository projektmitarbeiterRepo = new ProjektmitarbeiterRepository(); --> obsolet
	


	/**
	 * Schreibt das geänderte Projektmitarbeiter Objekt zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Projektmitarbeiterliste.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /projektmitarbeiter/aktualisieren
	 * 
	 * @return redirect nach Projektmitarbeiterliste
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		// eingegebene Daten wede mithilfe von WebHelper abgerufen.
		Projektmitarbeiter projektmitarbeiterDetail = ProjektmitarbeiterWebHelper.itemFromWeb(request);
		
		log.trace("POST /projekt/aktualisieren mit projektmitarbeiterDetail " + projektmitarbeiterDetail);
		
		//Speichern des Projektmitarbeiters
		ProjektmitarbeiterRepository.save(projektmitarbeiterDetail);
		//Redirect auf Projektmitarbeiter Liste
		response.redirect("/projektmitarbeiterAnzeigen");
		return null;
	}
}


