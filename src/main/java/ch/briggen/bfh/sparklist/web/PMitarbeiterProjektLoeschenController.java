package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.PMitarbeiterProjektRepository;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für Operationen um Zuweisung zwischen Projektmitarbeiter und Projekt zu löschen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class PMitarbeiterProjektLoeschenController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PMitarbeiterProjektLoeschenController.class);
		
	/**
	 * Löscht die Zuweisung von Projekt und Projektmitarbeiter mit den entsprechenden IDs in der Datenbank
	 * /pmitarbeiterprojekt/loeschen?pmid=987&pid=1 löscht die Zuweisung des Projektmitarbeiters 987 und des Projekts 1 aus der Datenbank
	 * 
	 * Hört auf GET /pmitarbeiterprojekt/loeschen
	 * 
	 * @return Redirect zurück zur Bearbeitung des Projekts
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String pmid = request.queryParams("pmid");
		String pid = request.queryParams("pid");
		log.trace("GET /pmitarbeiterprojekt/loeschen mit pmid und pid " + pmid + pid);
		
		Long longPmid = Long.parseLong(pmid);
		Long longPid = Long.parseLong(pid);
		PMitarbeiterProjektRepository.delete(longPmid,longPid);
			
		response.redirect("/projektpmbearbeiten?id=" + pid);
		return null;
	}
}


