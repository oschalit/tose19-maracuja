package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert Collection mit allen Projektmitarbeitern um diese in einer Liste darzustellen.
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen @editor Team Maracuja
 *
 */
public class ProjektmitarbeiterAnzeigenController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjektmitarbeiterAnzeigenController.class);

	// ProjektmitarbeiterRepository repository = new ProjektmitarbeiterRepository(); --> obsolet

	/**
	 *Liefert die Projektmitarbeiter Liste zurück
	 * @return Redirect zurück zum projektmitarbeiterAnzeigenTemplate
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Projektmitarbeiter werden geladen und die Collection dann für das Template unter dem namen "projektmitarbeiterListe" bereitgestellt
		//Das Template muss dann auch den Namen "projektmitarbeiterListe" verwenden.
		HashMap<String, Collection<Projektmitarbeiter>> model = new HashMap<String, Collection<Projektmitarbeiter>>();
		model.put("projektmitarbeiterListe", ProjektmitarbeiterRepository.getAll());
		return new ModelAndView(model, "projektmitarbeiterAnzeigenTemplate");
	}
}
