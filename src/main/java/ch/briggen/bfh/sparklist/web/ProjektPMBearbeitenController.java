package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller kommt zur Verwendung wenn Projekt neu angelegt wird oder Projekt bearbeitet wird.
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektPMBearbeitenController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjektPMBearbeitenController.class);
	
	/**
	 * Requesthandler zum Bearbeiten der Projektmitarbeiter / Projekt Zuweisungen
	 * Hört auf GET /projektpmbearbeiten
	 * @return redirect auf projektDetailPMAendernTemplate
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
			log.trace("GET /projektpmbearbeiten Zuweisung Projekt / Projektmitarbeiter für Projekt mit id " + idString); 
		 
			Long id = Long.parseLong(idString);
			Projekt i = ProjektRepository.getById(id);
			
			Collection<Projektmitarbeiter> pm = ProjektmitarbeiterRepository.getAlmostAll(id);			
			Collection<Projektmitarbeiter> pmp = ProjektmitarbeiterRepository.getFromProject(id);
			Collection<Projektmitarbeiter> notpmp = ProjektmitarbeiterRepository.notInProject(id);
			
						
			model.put("projektDetail", i);
			model.put("projektMitarbeiter", pm);
			model.put("mitarbeiterInProjekt", pmp);
			model.put("notMitarbeiterInProjekt", notpmp);
			
			return new ModelAndView(model, "projektDetailPMAendernTemplate");
		}
		
		
	}
	
	
	



