package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import spark.Request;

/**
 * Wird beim Bearbeiten und Anlegen von Projektmitarbeitern verwendet, um Daten aus Webformular auszulesen.
 * @author M. Briggen @editor Team Maracuja
 */	

class ProjektmitarbeiterWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjektmitarbeiterWebHelper.class);
	
	public static Projektmitarbeiter itemFromWeb(Request request)
	{
		return new Projektmitarbeiter(
				Long.parseLong(request.queryParams("projektmitarbeiterDetail.id")),
				request.queryParams("projektmitarbeiterDetail.vorname"),
				request.queryParams("projektmitarbeiterDetail.nachname"),
				request.queryParams("projektmitarbeiterDetail.email"),
				request.queryParams("projektmitarbeiterDetail.abteilung"),
				request.queryParams("projektmitarbeiterDetail.arbeitsort")
				);
	}

}
