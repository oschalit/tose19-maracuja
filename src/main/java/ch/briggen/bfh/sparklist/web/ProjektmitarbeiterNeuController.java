package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller um neuen Projektmitarbeiter in DB einzutragen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektmitarbeiterNeuController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektmitarbeiterNeuController.class);
		
	private ProjektmitarbeiterRepository itemRepo = new ProjektmitarbeiterRepository();
	
	/**
	 * Erstellt ein neues Projektmitarbeiter Objekt in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Projektmitarbeiter Liste redirected.
	 * 
	 * Hört auf POST /projektmitarbeiter/neu
	 * 
	 * @return Redirect zurück zur Projektmitarbeiter Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projektmitarbeiter projektmitarbeiterDetail = ProjektmitarbeiterWebHelper.itemFromWeb(request);
		log.trace("POST /projektmitarbeiter/neu mit projektmitareiterDetail " + projektmitarbeiterDetail);
		
		//Projektmitarbeiter wird in DB eingefügt
		itemRepo.insert(projektmitarbeiterDetail);
		
		
		// redirect auf Projektmitarbeiter Liste
		response.redirect("/projektmitarbeiterAnzeigen");
		return null;
	}
}


