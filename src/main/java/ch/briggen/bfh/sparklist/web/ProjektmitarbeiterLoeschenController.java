package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller um einzelnen Projektmitarbeiter zu löschen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektmitarbeiterLoeschenController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektmitarbeiterLoeschenController.class);
		
	private ProjektmitarbeiterRepository projektmitarbeiterRepo = new ProjektmitarbeiterRepository();
	

	/**
	 * Löscht das Projektmitarbeiter Objekt mit der übergebenen id in der Datenbank
	 * /projektmitarbeiter/loeschen&id=987 löscht das Projektmitarbeiter Objekt mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /projektmitarbeiter/loeschen 
	 * 
	 * @return Redirect zurück zur Projektmitarbeiter Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /projektmitarbeiter/loeschen mit id " + id);
		
		Long longId = Long.parseLong(id);
		projektmitarbeiterRepo.delete(longId);
		response.redirect("/projektmitarbeiterAnzeigen");
		return null;
	}
}


