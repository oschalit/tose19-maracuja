package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * 
 * Liefert Collection mit allen Projekten um diese in einer Liste darzustellen.
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen @editor Team Maracuja
 *
 */
public class ProjektAnzeigenController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjektAnzeigenController.class);

	ProjektRepository repository = new ProjektRepository();

	/**
	 *Liefert die Projekt-Liste zurück
	 * @return Redirect zu projektAnzeigenTemplate
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Projekte werden geladen und die Collection dann für das Template unter dem namen "ProjektListe" bereitgestellt
		//Das Template muss dann auch den Namen "projektListe" verwenden.
		HashMap<String, Collection<Projekt>> model = new HashMap<String, Collection<Projekt>>();
		model.put("projektListe", ProjektRepository.getAll());
		return new ModelAndView(model, "projektAnzeigenTemplate");
	}
}
