package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Abteilung;
import ch.briggen.bfh.sparklist.domain.AbteilungRepository;
import ch.briggen.bfh.sparklist.domain.Arbeitsort;
import ch.briggen.bfh.sparklist.domain.ArbeitsortRepository;
import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller kommt zur Verwendung wenn Projektmitarbeiter neu angelegt wird oder Projektmitarbeiter bearbeitet wird.
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektmitarbeiterBearbeitenController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjektmitarbeiterBearbeitenController.class);
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Projektmitarbeiters. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer Projektmitarbeiter erstellt (Aufruf von /projektmitarbeiter/neu)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars der Projektmitarbeiter mit der übergebenen id upgedated (Aufruf /projektmitarbeiter/aktualisieren) 
	 * Hört auf GET /projektmitarbeiter
	 * @return gibt den Namen des zu verwendenden Templates zurück. "projektmitarbeiterDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /projektmitarbeiter für INSERT mit id " + idString);
			//der Submit-Button ruft /projektmitarbeiter/neu auf --> INSERT
			model.put("postAction", "/projektmitarbeiter/neu");
			// Alle vorhandenen Abteilungen werden geladen, um diese in einem Drop Down anzuzeigen
			Collection<Abteilung> a = AbteilungRepository.getAll();
			Collection<Arbeitsort> ao = ArbeitsortRepository.getAll();
			
			model.put("abteilungDetail", a);
			model.put("arbeitsortDetail", ao);
			model.put("projektmitarbeiterDetail", new Projektmitarbeiter());
			
			//das Template projektmitarbeiterDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "projektmitarbeiterDetailNeuTemplate");

		}
		else
		{
			log.trace("GET /projektmitarbeiter für UPDATE mit id " + idString);
			//der Submit-Button ruft /projektmitarbeiter/aktualisieren auf --> UPDATE
			model.put("postAction", "/projektmitarbeiter/aktualisieren"); 
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projektmitarbeiterDetail" hinzugefügt. projektmitarbeiterDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Projektmitarbeiter i = ProjektmitarbeiterRepository.getById(id);
			Long abteilung = i.getAbteilungID();
			Long arbeitsort = i.getArbeitsortID();
			// Alle vorhandenen Abteilungen werden geladen, um diese in einem Drop Down anzuzeigen
			Collection<Abteilung> a = AbteilungRepository.getAlmostAll(abteilung);
			Collection<Arbeitsort> ao = ArbeitsortRepository.getAlmostAll(arbeitsort);
	
			model.put("projektmitarbeiterDetail", i);
			model.put("abteilungDetail", a);
			model.put("arbeitsortDetail", ao);
			
			//das Template projektmitarbeiterDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "projektmitarbeiterDetailTemplate");
		}
		
		
	}
	
	
	
}


