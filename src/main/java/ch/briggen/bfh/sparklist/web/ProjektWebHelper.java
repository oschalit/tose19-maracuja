package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projekt;
import spark.Request;

/**
 * Wird beim Bearbeiten und Erstellen von Projekten verwendet, um Daten aus Webformular auszulesen.
 * @author M. Briggen @editor Team Maracuja
 */

class ProjektWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjektWebHelper.class);
	
	public static Projekt itemFromWeb(Request request)
	{
		return new Projekt(
				Long.parseLong(request.queryParams("projektDetail.id")),
				request.queryParams("projektDetail.name"),
				request.queryParams("projektDetail.startdatum"),
				request.queryParams("projektDetail.enddatum"),
				Long.parseLong(request.queryParams("projektDetail.projektleiter"))
				);
	}

}
