package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.PMitarbeiterProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller um Projekte und Projektmitarbeiter einenader zuzuweisen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class PMitarbeiterProjektHinzufuegenController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PMitarbeiterProjektHinzufuegenController.class);
		
	// private ProjektRepository projektRepo = new ProjektRepository(); --> obsolet
	

	/**
	 * Erstellt die Zuweisung von Projekt und Projektmitarbeiter mit den entsprechenden IDs in der Datenbank
	 * /pmitarbeiterprojekt/hinzufuegen?pmid=987&pid=1 erstellt die Zuweisung des Projektmitarbeiters 987 und des Projekts 1 in der Datenbank
	 * 
	 * Hört auf GET /pmitarbeiterprojekt/hinzufuegen
	 * 
	 * @return Redirect zurück zur Bearbeitung des Projekts
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String pmid = request.queryParams("pmid");
		String pid = request.queryParams("pid");
		log.trace("GET /pmitarbeiterprojekt/hinzufuegen mit pmid und pid " + pid + " " + pmid);
		
		Long longPmid = Long.parseLong(pmid);
		Long longPid = Long.parseLong(pid);
		PMitarbeiterProjektRepository.insert(longPmid,longPid);

			
		response.redirect("/projektpmbearbeiten?id=" + pid);
		return null;
	}
}


