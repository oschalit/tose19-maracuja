package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller zum Löschen von Projekten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektLoeschenController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektLoeschenController.class);
		
	private ProjektRepository projektRepo = new ProjektRepository();
	

	/**
	 * Löscht das Projekt mit der übergebenen id in der Datenbank
	 * /projekt/loeschen&id=987 löscht das Projekt mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /projekt/loeschen
	 * 
	 * @return Redirect zurück zur Liste mit den Projekten
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /projekt/loeschen mit id " + id);
		
		Long longId = Long.parseLong(id);
		projektRepo.delete(longId);
		response.redirect("/projektAnzeigen");
		return null;
	}
}


