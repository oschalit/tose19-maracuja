package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller um Projekte zu aktualisieren
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektAktualisierenController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjektAktualisierenController.class);
		
	// private ProjektRepository projektRepo = new ProjektRepository(); --> obsolet
	


	/**
	 * Schreibt das geänderte Projekt zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Projektliste
	 * 
	 * Hört auf POST /projekt/aktualisieren
	 * 
	 * @return redirect nach /projektAnzeigen
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		//eingegebene Daten mit WebHelper abrufen
		Projekt projektDetail = ProjektWebHelper.itemFromWeb(request);
		
		log.trace("POST /projekt/aktualisieren " + projektDetail);
		
		//Speichern des Projekts
		ProjektRepository.save(projektDetail);
		
		//WIRD NICHT GEMACHT
		// response.redirect("/projektmitarbeiter?id="+projektmitarbeiterDetail.getId());
		
		//Redirect auf Projektliste
		response.redirect("/projektAnzeigen");
		return null;
	}
}


