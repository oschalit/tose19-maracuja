package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller um neue Projekte zu erstellen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektNeuController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektNeuController.class);
		
	private ProjektRepository projektRepo = new ProjektRepository();
	
	/**
	 * Erstellt ein neues Projekt Objekt in der DB.
	 * Bei Erfolg wird auf die Liste aller Projekte redirected. 
	 * 
	 * Hört auf POST /projekt/neu
	 * 
	 * @return Redirect zurück zur Projektliste.
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projekt projektDetail = ProjektWebHelper.itemFromWeb(request);
		log.trace("POST /projekt/neu mit projektDetail " + projektDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = projektRepo.insert(projektDetail);		
		
		//redirect auf Projektdetail Seite, damit noch Projektmitarbeiter dem Projekt zugewiesen werden können.
		response.redirect("/projektpmbearbeiten?id=" + id); 
		return null;
	}
}


