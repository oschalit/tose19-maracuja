package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller kommt zur Verwendung wenn Projekt neu angelegt wird oder Projekt bearbeitet wird.
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektBearbeitenController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjektBearbeitenController.class);
	
	/**
	 * Requesthandler zum Bearbeiten eines Projekts. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /projekt/neu)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars der Projektmitarbeiter mit der übergebenen id upgedated (Aufruf /projekt/aktualisieren) 
	 * Hört auf GET /projekt
	 * @return redirect auf ProjektDetailNeuTemplate
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /projekt für INSERT mit id " + idString);
			//der Submit-Button ruft /projekt/neu auf --> INSERT
			model.put("postAction", "/projekt/neu");
			model.put("projektDetail", new Projekt());
			// speichert alle Projektmitarbeiter um diese aus Auswahl für die PL in einem Drop Down darzustellen
			Collection<Projektmitarbeiter> pm = ProjektmitarbeiterRepository.getAll();
			model.put("projektMitarbeiter", pm);
			
			return new ModelAndView(model, "projektDetailNeuTemplate");

		}
		else
		{
			log.trace("GET /projekt für UPDATE mit id " + idString);
			//der Submit-Button ruft /projekt/aktualisieren auf --> UPDATE
			model.put("postAction", "/projekt/aktualisieren"); 
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projektDetail" hinzugefügt. projektDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Projekt i = ProjektRepository.getById(id);
			// speichert alle Projektmitarbeiter um diese aus Auswahl für die PL in einem Drop Down darzustellen
			Collection<Projektmitarbeiter> pm = ProjektmitarbeiterRepository.getAlmostAll(id);
			// speichert Mitarbeiter welche in Projekt sind und welche nicht in Projekt sind in separaten Collections um diese für die Zuweisung zu unterscheiden
			Collection<Projektmitarbeiter> pmp = ProjektmitarbeiterRepository.getFromProject(id);
			Collection<Projektmitarbeiter> notpmp = ProjektmitarbeiterRepository.notInProject(id);
					
			model.put("projektDetail", i);
			model.put("projektMitarbeiter", pm);
			model.put("mitarbeiterInProjekt", pmp);
			model.put("notMitarbeiterInProjekt", notpmp);
			
			return new ModelAndView(model, "projektDetailTemplate");
		}
		
		
	}
	
	
	
}


