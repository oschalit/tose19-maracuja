package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller kommt zur Verwendung wenn Projekt mit leserechten angeschaut wird.
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen @editor Team Maracuja
 *
 */

public class ProjektLesenController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjektLesenController.class);
	
	/**
	 * Requesthandler zum Lesen eines Projekts. 
	 * Liefert die schreibgeschützten Formulare des gewünschten Projekts.
	 * Hört auf GET /projektlesen
	 * @return Redirect auf projektDetailLesenTemplate
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
			log.trace("GET /projektlesen um es anzuzeigen mit id " + idString);
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projektDetail" hinzugefügt. projektDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Projekt i = ProjektRepository.getById(id);

			// Speichert alle Projektmitarbeiter von Projekt in Collection um diese anzuzeigen
			Collection<Projektmitarbeiter> pmp = ProjektmitarbeiterRepository.getFromProject(id);
			
				
			model.put("projektDetail", i);
			model.put("mitarbeiterInProjekt", pmp);
			
			return new ModelAndView(model, "projektDetailLesenTemplate");
		}
		
		
	
	
	
	
}
