package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Projektmitarbeiter;
import ch.briggen.bfh.sparklist.domain.ProjektmitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die Startseite der Applikation Projektmitarbeiter Verwaltung PROVER.
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen @editor Team Maracuja
 *
 */
public class ProjektmitarbeiterVRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjektmitarbeiterVRootController.class);

	ProjektmitarbeiterRepository repository = new ProjektmitarbeiterRepository();

	/**
	 *Liefert Startseite von PROVER
	 * @return Redirect zum startProjektmitarbeiterVTemplate
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Collection<Projektmitarbeiter>> model = new HashMap<String, Collection<Projektmitarbeiter>>();
		return new ModelAndView(model, "startProjektmitarbeiterVTemplate");
	}
}
